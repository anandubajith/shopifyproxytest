package com.storilabs.shopify.subscription_customer.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.storilabs.shopify.subscription_customer.service.GraphQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@RestController
public class CustomerController {
    @Autowired
    GraphQLQuery graphQLQuery;

    @RequestMapping(value="/login" , produces = {"application/liquid"})
    public ModelAndView handleLogin() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login.liquidpartial");
        return modelAndView;
    }

    @RequestMapping(value="/postlogin/{customerId}" , produces = {"application/liquid"})
    public ModelAndView onLogin(@PathVariable String customerId) {
        System.out.println("ON login");
        // todo generate unique token
        String projectUrl = "https://anandu.net";
        return new ModelAndView("redirect:" + projectUrl);
    }


    @RequestMapping(value = "/subscriptions",produces={"application/liquid"})
    public ModelAndView handleSubscriptionsList(@RequestParam Map<String, String> requestParams) {
        Map<String, Object> variables = new HashMap<>();
        variables.put("id", requestParams.get("userToken"));
        JsonNode data = graphQLQuery.query("fetchCustomerSubscriptions", variables).get("data");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("subscriptions.liquidpartial");
        modelAndView.addObject("data", data);
        return modelAndView;
    }

}
