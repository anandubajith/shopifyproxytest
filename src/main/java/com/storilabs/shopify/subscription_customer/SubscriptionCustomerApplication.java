package com.storilabs.shopify.subscription_customer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.nio.charset.StandardCharsets;

@SpringBootApplication
public class SubscriptionCustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubscriptionCustomerApplication.class, args);
	}

	@Bean
	public ClassLoaderTemplateResolver liquidTemplateResolver() {
		ClassLoaderTemplateResolver secondaryTemplateResolver = new ClassLoaderTemplateResolver();
		secondaryTemplateResolver.setPrefix("templates/");
		secondaryTemplateResolver.setTemplateMode(TemplateMode.HTML);
		secondaryTemplateResolver.setCharacterEncoding("UTF-8");
		secondaryTemplateResolver.setOrder(2);
		secondaryTemplateResolver.setCheckExistence(true);
		return secondaryTemplateResolver;
	}
	@Bean
	public ThymeleafViewResolver liquidViewResolver(SpringTemplateEngine templateEngine){
		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		viewResolver.setContentType("application/liquid");
		viewResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
		viewResolver.setOrder(1);
		viewResolver.setViewNames(new String[] {"*.liquidpartial"});
		viewResolver.setTemplateEngine(templateEngine);
		return viewResolver;
	}
}
