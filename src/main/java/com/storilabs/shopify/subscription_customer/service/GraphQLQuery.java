package com.storilabs.shopify.subscription_customer.service;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
public class GraphQLQuery {
    private Map<String, String> QUERIES = new HashMap();
    private static String SHOPIFY_ACCESS_TOKEN = "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";
    private static String SHOPIFY_API_URL = "https://storilabs.myshopify.com/admin/api/2021-01/graphql.json";

    public GraphQLQuery() throws IOException {
        // read all .graphql files from resources
        ClassLoader cl = this.getClass().getClassLoader();
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(cl);
        Resource[] resources = resolver.getResources("classpath*:/graphql/*.graphql");
        for (Resource resource : resources) {
            System.out.println("Loaded graphQL Query file" + resource.getFilename());
            BufferedInputStream bis = new BufferedInputStream(resource.getInputStream());
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            for (int result = bis.read(); result != -1; result = bis.read()) {
                buf.write((byte) result);
            }
            QUERIES.put(resource.getFilename(), buf.toString("UTF-8"));
        }
    }

    public ObjectNode query(String queryFileName, Map<String, Object> variables) {
        String queryFilePath = queryFileName + ".graphql";
        if (!QUERIES.containsKey(queryFilePath)) {
            throw new Error("Invalid query");
        }
        String rawQuery = QUERIES.get(queryFilePath);
        try {
            Map<String, Object> requestBody = new HashMap<>();
            requestBody.put("query", rawQuery);
            requestBody.put("variables", variables);
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("X-Shopify-Access-Token", SHOPIFY_ACCESS_TOKEN);
            HttpEntity<Object> entity = new HttpEntity<>(requestBody, headers);
            ObjectNode response = restTemplate.postForObject(SHOPIFY_API_URL, entity, ObjectNode.class);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
