package com.storilabs.shopify.subscription_customer.filter;

import org.apache.tomcat.util.buf.HexUtils;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.stereotype.Component;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ShopifyRequestFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        System.out.printf("Logging Request  %s : %s\n", req.getMethod(), req.getRequestURI());
        // verify that request is from shopify
        String message = convertQueryStringToMessage(req);
        String computedHMAC = calcHmacSha256(message, "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");
        System.out.printf("%s and %s\n", request.getParameter("signature"), computedHMAC);
        if (computedHMAC.equals(req.getParameter("signature"))) {
            System.out.println("Matched successfully, so do request");
        } else {
            System.out.println("Invalid hmac value");
        }
        chain.doFilter(request, response);
        // maybe move setContentType to here
        res.setContentType("application/liquid");
        res.setCharacterEncoding(StandardCharsets.UTF_8.name());
        System.out.printf("Logging Response :%s", res.getContentType());
    }

    static String convertQueryStringToMessage(HttpServletRequest httpServletRequest) {
        Map<String, String> queryParams = Collections.list(httpServletRequest.getParameterNames())
                .stream()
                .collect(
                        Collectors.toMap(parameterName -> parameterName, (parameterName)
                                -> String.join(",", httpServletRequest.getParameterValues(parameterName))
                        )
                );
        queryParams.remove("signature");
        return queryParams
                .keySet()
                .stream()
                .map((key) -> String.format("%s=%s", key, queryParams.get(key)))
                .sorted()
                .collect(Collectors.joining());
    }

    static public String calcHmacSha256(String message, String secretKey) {
        byte[] hmacSha256 = null;
        try {
            Mac mac = Mac.getInstance("HmacSHA256");
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
            mac.init(secretKeySpec);
            hmacSha256 = mac.doFinal(message.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            throw new RuntimeException("Failed to calculate hmac-sha256", e);
        }
        return HexUtils.toHexString(hmacSha256);
    }

}
